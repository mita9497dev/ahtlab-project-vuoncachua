# Vườn Cà Chua

Một số hướng dẫn khi sử dụng code VuonCaChua

## Thư viện

- Nằm trong thư mục src/libraries
- Gồm: OLED, DHT11
- Thư viện **Blynk** tải từ nguồn của **Blynk**

## Code

- Nằm trong thư mục src/main

## Cài đặt

- Chỉnh lại WiFi

``` c++

char auth[] = "KWtwsjgfPQicFtt5hnW_-nFOPy_9WkNH";

char ssid[] = "YourNetworkName";

char pass[] = "YourPassword";

```

- Blynk 

``` c++

#define RELAY_1_BLYNK V11 // RELAY 1
#define RELAY_2_BLYNK V12 // RELAY 2
#define RELAY_3_BLYNK V13 // RELAY 3

#define SOL_BLYNK V1	// AM DAT
#define HUL_BLYNK V2	// DO AM 
#define TEM_BLYNK V3	// NHIET DO 
#define SUN_BLYNK V4	// ANH SANG
#define RAI_BLYNK V5	// MUA

```

- Các khai báo khác chú ý phần comment
