/* ============================= */
/* BLYNK */ 
/* ============================= */
#define BLYNK_PRINT Serial

#include <WiFi.h>
#include <WiFiClient.h>
#include <BlynkSimpleEsp32.h>

char auth[] = "KWtwsjgfPQicFtt5hnW_-nFOPy_9WkNH";
char ssid[] = "Px3";
char pass[] = "minh15141347";

BlynkTimer timer;

/* ============================= */
/* RELAY */ 
/* ============================= */
#define RELAY_1 4
#define RELAY_2 5
#define RELAY_3 12

#define RELAY_1_BLYNK V11
#define RELAY_2_BLYNK V12
#define RELAY_3_BLYNK V13

/* ============================= */
/* SENSOR */ 
/* ============================= */
#define SOL_PIN 23 // am dat
#define DHT_PIN 15 // dht
#define RAI_PIN 14 // nhiet do dht
#define SUN_PIN 13 // anh sang

#define SOL_BLYNK V1
#define HUL_BLYNK V2
#define TEM_BLYNK V3
#define SUN_BLYNK V4
#define RAI_BLYNK V5

#include "DHT.h"
#define DHTTYPE DHT11
DHT dht(DHT_PIN, DHTTYPE);

int sol = 0, sun = 0, rai = 0;
float hul = 0, tem = 0;

/* ======================================== */
/* OLED */
/* ======================================== */
#include <Wire.h> 
#include "SSD1306Wire.h"

#define DISPLAY_WIDTH 128
#define DISPLAY_HEIGHT 64

#define SDA 21
#define SCL 22  

SSD1306Wire display(0x3c, SDA, SCL);

/* ============================= */
/* BTN */ 
/* ============================= */
#define BTN_1 18
#define BTN_2 19
#define BTN_3 25

#define PRESS_STATE false

#define SETTING_AUTO_BLYNK V9
bool settingAuto = false;

void setup()
{
    Serial.begin(115200);
    delay(1000);

    Blynk.begin(auth, ssid, pass);
    timer.setInterval(1000L, Sensor_Read);

    delay(5000);

    Oled_Init();
    Sensor_Init();
    BTN_Init();
    Relay_Init();
}

void loop()
{
    BTN_Read();
    
    Blynk.run();
    timer.run();
}

BLYNK_CONNECTED() 
{
    Blynk.syncAll();
}

// Setting AUTO
BLYNK_WRITE(SETTING_AUTO_BLYNK)
{
    Serial.print(F("V9: "));
    Serial.println(param.asInt());

    settingAuto = param.asInt();
}

BLYNK_WRITE(RELAY_1_BLYNK)
{
    Serial.print(F("V11: "));
    Serial.println(param.asInt());

    if (settingAuto)
    {
        Blynk.virtualWrite(RELAY_1_BLYNK, !param.asInt());
        return;
    }
    
    digitalWrite(RELAY_1, param.asInt());
}

BLYNK_WRITE(RELAY_2_BLYNK)
{
    Serial.print(F("V12: "));
    Serial.println(param.asInt());

    if (settingAuto)
    {
        Blynk.virtualWrite(RELAY_2_BLYNK, !param.asInt());
        return;
    }

    digitalWrite(RELAY_2, param.asInt());
}

BLYNK_WRITE(RELAY_3_BLYNK)
{
    Serial.print(F("V13: "));
    Serial.println(param.asInt());

    if (settingAuto)
    {
        Blynk.virtualWrite(RELAY_3_BLYNK, !param.asInt());
        return;
    }

    digitalWrite(RELAY_3, param.asInt());
}

void Oled_Init()
{
    display.init();
    display.flipScreenVertically();
    display.setFont(ArialMT_Plain_16);
    display.setTextAlignment(TEXT_ALIGN_LEFT);

    display.clear();
    display.drawString(16, 0, "SETUP WIFI");
    display.drawString(3, 20, ssid);
    display.drawString(3, 40, pass);
    display.display();
}

void Sensor_DisplayOled()
{
    display.clear();
    
    display.drawString(0, 0, "dat:");
    display.drawString(25, 0, String((int)sol));

    display.drawString(60, 0, "mua:");
    display.drawString(95, 0, String((int)rai));

    display.drawString(0, 20, "sun:");
    display.drawString(30, 20, String((int)sun));

    display.drawString(70, 20, "t:");
    display.drawString(86, 20, String((int)tem));

    display.drawString(0, 40, "h:");
    display.drawString(15, 40, String((int)hul));
    
    display.display();
}

void Sensor_Init()
{
    pinMode(SOL_PIN, INPUT);
    pinMode(SUN_PIN, INPUT);
    pinMode(RAI_PIN, INPUT);

    // DHT11
    dht.begin();
}

void Sensor_Read()
{
    sol = readPinDigital(SOL_PIN);
    sun = readPinDigital(SUN_PIN);
    rai = readPinDigital(RAI_PIN);

    Sensor_Debug("sol", sol, SOL_PIN);
    Sensor_Debug("sun", sun, SUN_PIN);
    Sensor_Debug("rai", rai, RAI_PIN);
    
    // DHT11
    hul = dht.readHumidity();
    tem = dht.readTemperature();
    Sensor_Debug("hul", (int)hul, DHT_PIN);
    Sensor_Debug("tem", (int)tem, DHT_PIN);

    Sensor_WriteBlynk();
    Sensor_DisplayOled();

    if (settingAuto)
    {
        processConditions();   
    }
}

void Sensor_WriteBlynk()
{
    Blynk.virtualWrite(SOL_BLYNK, sol);
    Blynk.virtualWrite(SUN_BLYNK, sun);
    Blynk.virtualWrite(RAI_BLYNK, rai);

    // DHT11
    if (!isnan(hul) && !isnan(tem)) 
    {
        Blynk.virtualWrite(HUL_BLYNK, hul);
        Blynk.virtualWrite(TEM_BLYNK, tem);
    }
}

void Sensor_Debug(String name, int val, int pin)
{
    Serial.println(name + " - pin: " + String(pin) + " - val: " + String(val));
}

void Relay_Init()
{
    pinMode(RELAY_1, OUTPUT);
    pinMode(RELAY_2, OUTPUT);
    pinMode(RELAY_3, OUTPUT);
}

void BTN_Read()
{
    if (readPinDigital(BTN_2, PRESS_STATE))
    {
        delay(200);
        digitalWrite(RELAY_1, !digitalRead(RELAY_1));
        Blynk.virtualWrite(RELAY_1_BLYNK, digitalRead(RELAY_1));
    }

    if (readPinDigital(BTN_1, PRESS_STATE))
    {
        delay(200);
        digitalWrite(RELAY_3, !digitalRead(RELAY_3));
        Blynk.virtualWrite(RELAY_3_BLYNK, digitalRead(RELAY_3));
    }

    if (readPinDigital(BTN_3, PRESS_STATE))
    {
        delay(200);
        digitalWrite(RELAY_2, !digitalRead(RELAY_2));
        Blynk.virtualWrite(RELAY_2_BLYNK, digitalRead(RELAY_2));
    }
}

void BTN_Init()
{
    pinMode(BTN_1, INPUT);
    pinMode(BTN_2, INPUT);
    pinMode(BTN_3, INPUT);
}

void processConditions()
{
    // DHT11
    hul = dht.readHumidity();
    tem = dht.readTemperature();

    if (hul < 60)
    {
        digitalWrite(RELAY_3, true);
        Blynk.virtualWrite(RELAY_3_BLYNK, true);
    }
    else if (hul > 80)
    {
        digitalWrite(RELAY_3, false);
        Blynk.virtualWrite(RELAY_3_BLYNK, false);
    }

    if (tem > 28) 
    {
        digitalWrite(RELAY_2, true);
        Blynk.virtualWrite(RELAY_2_BLYNK, true);
    }
    else if (tem < 20)
    {
        digitalWrite(RELAY_2, false);
        Blynk.virtualWrite(RELAY_2_BLYNK, false);
    }

    if (sun)
    {
        digitalWrite(RELAY_1, true);
        Blynk.virtualWrite(RELAY_1_BLYNK, true);
    }
    else
    {
        digitalWrite(RELAY_1, false);
        Blynk.virtualWrite(RELAY_1_BLYNK, false);
    }
}

bool readPinDigital(int pin, bool state)
{
    if (digitalRead(pin) == state) 
    {
        delay(50);
        if (digitalRead(pin) == state)
        {
            return true;
        }
    }
    
    return false;
}

bool readPinDigital(int pin)
{
    if (digitalRead(pin) == true) 
    {
        delay(50);
        if (digitalRead(pin) == true)
        {
            return true;
        }
    }
    
    return false;
}

void writePinDigital(int pin, int value)
{
    digitalWrite(pin, value);
}
